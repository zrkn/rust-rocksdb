#include "rocksdb/include/rocksdb/c.h"

#ifdef __cplusplus
extern "C" {
#endif

extern ROCKSDB_LIBRARY_API void rocksdb_options_set_max_subcompactions(
    rocksdb_options_t*, int);

#ifdef __cplusplus
}  /* end extern "C" */
#endif
