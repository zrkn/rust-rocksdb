#include "my_c.h"
#include "rocksdb/options.h"

using rocksdb::Options;

struct rocksdb_options_t         { Options           rep; };

void rocksdb_options_set_max_subcompactions(rocksdb_options_t* opt, int n) {
  opt->rep.max_subcompactions = n;
}
